# vue-weather

[Live demo](https://vue-weather-now.herokuapp.com/)

## Running and Building

``` bash
# install dependencies
yarn install

# Compiles and minifies for production
yarn build

# Run server
yarn start

# Compiles and hot-reloads for development
yarn serve

# Lints and fixes files
yarn lint
```

You have to run server with next env variables:
 - APIXU_TOKEN
 - GOOGLE_API_TOKEN 
