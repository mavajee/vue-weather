const path = require('path');
const express = require('express');

const router = require('./router');

const PORT = process.env.NODE_ENV === 'production' ? process.env.PORT : 51000;
const app = express();

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, '..', 'dist')));
}

app.use(router);
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
