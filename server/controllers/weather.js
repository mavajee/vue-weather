const axios = require('axios');

const { APIXU_TOKEN } = require('./../config');

/**
 * @see https://www.apixu.com/api-explorer.aspx
 */
module.exports = {
  async index(req, res) {
    const { latlng } = req.query;
    const url = `http://api.apixu.com/v1/forecast.json?key=${APIXU_TOKEN}&days=7&q=${latlng}`;

    const response = await axios.get(url);

    res.json(response.data);
  },
};
