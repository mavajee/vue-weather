const axios = require('axios');

const { GOOGLE_API_TOKEN } = require('./../config');

const googleApi = axios.create({
  baseURL: 'https://maps.googleapis.com/maps/api/',
});

module.exports = {
  async autocomplete(req, res) {
    const { q } = req.query;
    const url = `place/autocomplete/json?types=(cities)&input=${q}&key=${
      GOOGLE_API_TOKEN
    }`;

    const response = await googleApi.get(encodeURI(url));

    const data = response.data.predictions.map(prediction => ({
      description: prediction.description,
      placeId: prediction.place_id,
    }));

    res.json(data);
  },

  async coordsByPlaceId(req, res) {
    const { placeid } = req.query;
    const url = `place/details/json?input=bar&placeid=${placeid}&key=${GOOGLE_API_TOKEN}`;

    const response = await googleApi.get(encodeURI(url));

    const coords = {
      lat: response.data.result.geometry.location.lat,
      lng: response.data.result.geometry.location.lng,
    };

    res.json(coords);
  },

  async placeNameByCoords(req, res) {
    const { latlng } = req.query;
    const url = `/geocode/json?result_type=locality&latlng=${latlng}&key=${
      GOOGLE_API_TOKEN
    }`;

    const response = await googleApi.get(encodeURI(url));

    res.json(response.data.results[0].formatted_address);
  },
};
