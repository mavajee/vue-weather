const express = require('express');

const placesController = require('./controllers/places');
const weatherController = require('./controllers/weather');

const router = express.Router();

router.get('/api/places/autocomplete', placesController.autocomplete);
router.get('/api/places/coords-by-placeid', placesController.coordsByPlaceId);
router.get('/api/places/place-by-coords', placesController.placeNameByCoords);
router.get('/api/weather', weatherController.index);

module.exports = router;
