import Vue from 'vue';
import App from './App.vue';
import store from './store';

import './assets/styles/index.scss';

Vue.config.productionTip = false;

// store.dispatch('init');

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
