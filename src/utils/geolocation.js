/**
 * @returns {Promise}
 */
export const getCurrentPosition = () => new Promise((resolve, reject) => {
  const options = {
    enableHighAccuracy: true,
    maximumAge: 30000,
    timeout: 10000,
  };

  if (typeof navigator.geolocation === 'undefined') {
    reject();
    return;
  }

  const handleSuccess = (position) => {
    resolve({
      lat: position.coords.latitude,
      lng: position.coords.longitude,
    });
  };

  const handleError = () => {
    reject();
  };

  navigator.geolocation.getCurrentPosition(handleSuccess, handleError, options);
});
