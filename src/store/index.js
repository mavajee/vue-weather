import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import weather from './modules/weather';
import places from './modules/places';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: { places, weather },

  plugins: [createPersistedState()],

  state: {
    isLoaded: false,
    currentPosition: {
      name: undefined,
      coords: {},
    },
  },

  mutations: {
    SET_CURRENT_POSITION(state, { name, coords }) {
      state.currentPosition = { name, coords };
    },
    SET_IS_LOADED(state, isLoaded) {
      state.isLoaded = isLoaded;
    },
  },

  actions: {
    async setCurrentPositionAndLoadWeather({ commit, dispatch }, position) {
      commit('SET_CURRENT_POSITION', position);
      await dispatch('weather/loadWeather', position.coords);
      commit('SET_IS_LOADED', true);
    },
  },
});
