import { fetchWeather } from '@/services/api/weather';

const initialState = {
  weather: {
    location: {},
    current: {
      temp_c: '–',
      feelslike_c: '–',
      condition: { text: '▅▅▅▅', icon: undefined },
    },
    forecast: {
      forecastday: Array(7).fill({
        date_epoch: '▅▅',
        day: {
          condition: { text: '▅▅▅▅', icon: undefined },
          maxtemp_c: '–',
          mintemp_c: '–',
        },
      }),
    },
  },
};

const mutations = {
  SET_WEATHER(state, weather) {
    state.weather = weather;
  },
};

const actions = {
  async loadWeather({ commit }, coords) {
    const weather = await fetchWeather(coords);

    commit('SET_WEATHER', weather);
  },
};

export default {
  namespaced: true,
  state: initialState,
  mutations,
  actions,
};
