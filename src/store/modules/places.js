const initialState = {
  places: [],
};

const COUNT_SAVED_PLACES = 5;

function filterPlaces(places, placeId) {
  return places.filter(place => place.placeId !== placeId);
}

const mutations = {
  ADD_PLACE(state, place) {
    // add to the end and exclude duplicates
    let places = [...state.places];
    places = places.slice(-COUNT_SAVED_PLACES);
    places = filterPlaces(places, place.placeId);
    places.push(place);

    state.places = places;
  },
  REMOVE_PLACE_BY_ID(state, placeId) {
    state.places = filterPlaces(state.places, placeId);
  },
};

export default {
  namespaced: true,
  state: initialState,
  mutations,
};
