import axios from 'axios';

export async function fetchWeather({ lat, lng }) {
  const response = await axios.get(`/api/weather?latlng=${lat},${lng}`);

  return response.data;
}
