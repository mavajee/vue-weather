import axios from 'axios';

export async function autocomplete(cityName) {
  const response = await axios.get(`/api/places/autocomplete?q=${cityName}`);

  return response.data;
}

export async function fetchCoordsByPlaceId(placeId) {
  const response = await axios.get(`/api/places/coords-by-placeid?placeid=${placeId}`);

  return response.data;
}

export async function fetchPlaceNameByCoords({ lat, lng }) {
  const response = await axios.get(`/api/places/place-by-coords?latlng=${lat},${lng}`);

  return response.data;
}
